# Git !0!

**Version 1.5.1**

**Date 10/15/18**

**Author: Huu Vu**

## Basic Git Commands to get through life

- ``git status``

	This tells you the current status of your working directory.
	It will list all files or folders that changed since last 
	commit or files that are added. It will list some helpful commands
	sometime too.

- ``git log``

	 This display the commit history with its ID. Some parameters

	* ``git log --oneline``

			Display the commit ID and the commit message in one line

	* ``git log --pretty``

			Make the display look pretty (this should be enable by default)

	* ``git log --graph``
		
			This create an ASCII graph of all the merges. Looks cool on complex projects

	You can use multiple of these parameter as once like
	``git log --oneline --graph`` to make a graph but each commit
	only spans one line

- ``git pull``

	This is essential broken into two parts, git fetch and git merge
	Basically, it will update your working directory with the latest 
	file updates. 

	* **If you see no updates or git tells you there is nothing to do
	when you know there is an update, first check if you are on the 
	correct branch (git branch). Then check if the person uploaded
	correctly to the right branch.**

	* **If you are getting an error in your pull (i.e. there are unsaved
	changes that are not committed) make sure you commit the changes
	first then try pull again. If you do not want to keep your change
	do a git reset as shown in the advance section**

	* **If all fails, make sure the Zombie Apocalypse is not happening**

- ``git add <FileName>``

	This tells git to add a file be committed. You can input a path
	to the file if needed
	Some parameters

	- ``git add -A``

			Add every files in the current directory and subdirectory to git. Never use this please. We have a lot of debug files that makes the repo uselessly big

- ``git commit``

	This tells git to accept the changed add and create an instance
	of the directories and file. You will then need to write a commit
	message. Please write a helpful commit message. This will create
	a local commits that exist only on your computer.
	Some parameters

	- ``git commit -a``

			Tells git to add all tracked files and commit. Use this to quickly commit a change to a file previously committed without using git add <FileName>

	- ``git commit -m "<Message>"``

			This is the same as using git commit then writing the message. In this case, you write the message on the command line itself

	You can use git -a and git -m together to quickly add all tracked
	file and write a message

	EX: ``git commit -a -m "update Arduino files"``

- ``git push``

	Upload the commits to the git repo so others can download the changes. 
	**_Make sure you push if you want others to get your changes_**

	* You do not need to always push every commit. You can push a set 
	of commits. This is so you can make multiple small changes and do 
	multiple small commits locally that won't affect the main repo.
	If you mess up on your local git, it is easy to revert it locally
	then from the repo itself. Beside if all your commits are bad,
	you can quickly go back to the repo version that you know works
	
	It is strongly recommended that you ensure that you will make no 
	other reverts or resets once you push to remote. All of your changes 
	should be local so that it is easier to modify, edit, merge, or
	revert. Once you push, it gets a whole lot harder as master does not
	like you taking back what you give it.

- ``git branch``

	Tells you want branch you are on. Some parameters

	- ``git branch <Name>``

			Create a local branch under that name

	- ``git branch -d <Name>``

			Deletes a branch under that name

	You should always work on a branch. Never make actual changes on master
	unless it is a super minor change (i.e. typo). Once you make a change
	and confirm that everything works on master, it is in your best 
	interest to immediately commit and push the changes so there won't be
	a merge conflict on your side.
	
	If you work on a branch, always do a ``git pull`` on master and then
	``git pull master`` **ON YOUR BRANCH**. All merges should be master to
	your branch then (once all conflicts are resolved) your branch merged to 
	master then pushed. The branch master should always contain stable code
	and should never have merge conflicts. **It is your fault if master fails,
	especially if you did a merge to master**. If you attempt to push master 
	once merging completes and there is a pull request, repeat the merge 
	process again! **DO NOT BE LAZY**

- ``git checkout <BranchName>``

	This will switch your current directory to a branch. Your directory
	will now look exactly like the branch you switch to. All of your
	files and edits are gone except those that are on the branch. However,
	your files and edit still is in the previous branch you worked on.

	* **Git will yell at you if you edit a file and tries to checkout to 
		another branch without committing. You must either commit or reset
		the changes before switching branch. There is stashing, but we don't
		care for that now**

	* **If all fails, make sure you have a bomb shelter ready; just cause**

## Advance Git Commands to destroy life

- ``git checkout <Destination> <FileName>``

	Some Destinataions

	* ``HEAD~``

			This will revert your commit back to the current head 

	* ``<CommitID>``

			This will revert your commit back to the commit with the ID you provided

	This will revert the file changes to the destination's copy. You can effectively use
	this to return to a previous commit state by checking out all the fiels and doing a
	commit. This method will ensure there's no conflict with reverting back to a 
	previous state.

- ``git checkout <CommitID>``

	You will be in a deatched HEAD state where you cannot make any commits or changes
	until you create a branch out of this commit. This is more of a lookback method 
	where you can view the state of the project at this commit.

	Once you make a branch of this commit, feel free to treat this as like any other branch.
	You can even merge this back to master to go back to a commit"
	
	To get out of this state, just ``git checkout master``
	
	
- ``git revert``
	
	This will revert one commit back. Make sure you are doing this on on local changes.
	If you do this on pushed changes, **there may be conflicts**.
	
	You can even go back to previous commits or X commits back, but that can get tricky.

- ``git reset <Destination>``

	Some Destinations

	``HEAD~ ``

		This will revert your commit back to the current head 

	``<CommitID>``

		This will revert your coommit back to the commit with the ID you provided

	This will uncommit your LOCAL COMMITS to the current commit on the repo.
	You can then edit the files as needed.

		i.e. Say you did edit a file and did a git add and git commit, but
			you realized that the commit is bad because it breaks the whole
			system. So you obviously do not want to push commit to the repo.
			To uncommit the commit (your changes are still there) do a git
			reset HEAD~. Afterwarad change the file and recommit

	Some **ADVANCE COMMAND**. Use at your own risk

	- ``git reset --hard HEAD~``

			WARNING: This will remove all your changes. Your directories will look exactly like the repo. 

	- ``git reset --hard <CommitID>``

			WARNING: This will remove all your changes.  DO NOT DO THIS Your directories will look exactly like when the commit ID was made

- ``git commit --amend``

	This allows you to fix your previous commit message. Don't try to amend
	a push commit, it causes trouble if someone already push to remote. If
	you must do an amend, do a pull first.

- ``git merge <Branch>``

	This will merge/combine the Branch you specified to the current branch.
	Typically, this is when you create an off branch to do some quick fixing
	and now what to commit it to the parent branch.

	* **You may run into a problem where git is unable to merge the branches
	and so you need to manually edit the files to incorporate the changes you
	want and the changed other made also.**

	**_PLEASE_** talk with the person who made the conflicting merge before editing 
	the files and recommitting. Once you are done fixing the files, add the 
	files again with ``git add`` or ``git commit -a``. Really make sure you remove
	all the merge conflict info lines in the code base itself!

- ``git push -f``

	**WARNING WARNING WARNING**

		If you every use this for no good reason, I will have to report you	for an act of unauthorized deletion of files
		
		Never use this without my permission or unless you are sure you know what you are doing. 

	This command is the mother of all time travelers. This is overwrite all of
	git's history with your current local history. This will remove all files
	and edit that is not on your current directory. This will remove all
	commits that is not on your current git history. This is almost
	unrecoverable. It takes time and effort to recover from this command.

	You should have almost no reason to use this command. If you ever make
	a mistake and want to hide it, just do a recommit instead of using this 
	command because one wrong mistake means that a person commit will be erased
	immediately. It will not ask you for a confirmation or display any errors
	or warning. Git will just rewrite history PERIOD

## History:
- 10/15/18
	- Fix typo
	- Add some more comments and notes
	- Update to Markdown
	- Add more detail on checkout files/commit and revert

- 3/7/17
	- Initial Commit